<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" units="mm" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>urbina_poligono</se:Name>
    <UserStyle>
      <se:Name>urbina_poligono</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>"> 60 METROS"</se:Name>
          <se:Description>
            <se:Title>"> 60 METROS"</se:Title>
          </se:Description>
          <!--Parser Error: syntax error, unexpected EQ, expecting '(' - Expression was: area = '"> 60 METROS"'-->
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#00a2ea</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">0.26</se:SvgParameter>
              <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>"0-20 METROS"</se:Name>
          <se:Description>
            <se:Title>"0-20 METROS"</se:Title>
          </se:Description>
          <!--Parser Error: syntax error, unexpected EQ, expecting '(' - Expression was: area = '"0-20 METROS"'-->
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#bdd2ff</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">0.26</se:SvgParameter>
              <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>"20 - 50 METROS"</se:Name>
          <se:Description>
            <se:Title>"20 - 50 METROS"</se:Title>
          </se:Description>
          <!--Parser Error: syntax error, unexpected EQ, expecting '(' - Expression was: area = '"20 - 50 METROS"'-->
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#97dbf2</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">0.26</se:SvgParameter>
              <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>"50 - 60 METROS"</se:Name>
          <se:Description>
            <se:Title>"50 - 60 METROS"</se:Title>
          </se:Description>
          <!--Parser Error: syntax error, unexpected EQ, expecting '(' - Expression was: area = '"50 - 60 METROS"'-->
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#00c7c2</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">0.26</se:SvgParameter>
              <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>"COSTA NORTE"</se:Name>
          <se:Description>
            <se:Title>"COSTA NORTE"</se:Title>
          </se:Description>
          <!--Parser Error: syntax error, unexpected EQ, expecting '(' - Expression was: area = '"COSTA NORTE"'-->
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#f7c468</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">0.26</se:SvgParameter>
              <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>"COSTA SUR"</se:Name>
          <se:Description>
            <se:Title>"COSTA SUR"</se:Title>
          </se:Description>
          <!--Parser Error: syntax error, unexpected EQ, expecting '(' - Expression was: area = '"COSTA SUR"'-->
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#f7c468</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">0.26</se:SvgParameter>
              <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name></se:Name>
          <se:Description>
            <se:Title>area is ''</se:Title>
          </se:Description>
          <!--Parser Error: syntax error, unexpected EQ, expecting '(' - Expression was: area = ''-->
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#c6d17d</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">0.26</se:SvgParameter>
              <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
